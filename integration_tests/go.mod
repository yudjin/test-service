module bitbucket.org/yudjin/test-service/integration_tests

go 1.15

require (
	bitbucket.org/yudjin/test-service v0.0.0-20211019141708-4c5a1d4fff28
	github.com/sirupsen/logrus v1.9.0
	github.com/stretchr/testify v1.8.0
	google.golang.org/grpc v1.49.0
)
