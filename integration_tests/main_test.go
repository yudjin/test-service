package integration_tests

import (
	"bitbucket.org/yudjin/test-service/internal/views/grpcsumservice"
	"context"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"google.golang.org/grpc"
	"net/http"
	"os"
	"testing"
	"time"
)

const delay = 5 * time.Second

var grpcClient grpcsumservice.SumServiceClient
var restClient http.Client

func TestMain(m *testing.M)  {
	logrus.Infof("wait %s for service availability...", delay)
	time.Sleep(delay)

	sumServiceConnection, err := grpc.Dial("api:9090",
		grpc.WithDisableRetry(),
		grpc.WithInsecure(),
		grpc.WithBlock(),
	)
	if err != nil {
		logrus.Fatal(err)
	}
	grpcClient = grpcsumservice.NewSumServiceClient(sumServiceConnection)
	defer sumServiceConnection.Close()

	status := 0

	if st := m.Run(); st > status {
		status = st
	}
	os.Exit(status)
}

func Test(t *testing.T)  {
	t.Run("grpc sum", func(t *testing.T) {
		resp, err := grpcClient.SumOfTwoNumbers(context.Background(), &grpcsumservice.SumNumbersRequest{
			FirstNumber:  2,
			SecondNumber: 2,
		})
		assert.NoError(t, err)
		assert.EqualValues(t, resp.SumNumbers, 2+2)
	})
}