GO_SWAGGER = $(shell curl -s https://api.github.com/repos/go-swagger/go-swagger/releases/latest | jq -r .tag_name)
build-linux:
	env GOOS=linux GOARCH=arm go build -o api internal/cmd/api/main.go

build:
	go build -o api internal/cmd/api/main.go

run:
	go build -o api internal/cmd/api/main.go && CONFIGOR_DEBUG_MODE=true ./api

swagger-api:
	@if [ ! -f "./swagger-gen" ]; then\
		curl -o ./swagger-gen -L'#' https://github.com/go-swagger/go-swagger/releases/download/$(GO_SWAGGER)/swagger_$(shell echo `uname`|tr '[:upper:]' '[:lower:]')_amd64; chmod +x ./swagger-gen;\
	fi
	./swagger-gen generate spec -w ./internal/cmd/api -o ./swagger.json;\

test-integration:
	set -e ;\
	docker-compose -f docker-compose.yml up --build -d ;\
	test_status_code=0 ;\
	docker-compose -f docker-compose.yml run integration_tests go test || test_status_code=$$? ;\
	docker-compose -f docker-compose.yml down ;\
	exit $$test_status_code ;

test-cleanup:
	docker-compose -f docker-compose.yml down \
        --rmi local \
		--volumes \
		--remove-orphans \
		--timeout 60; \
  	docker-compose rm -f

up:
	docker-compose -f docker-compose.yml up --build -d

down:
	docker-compose -f docker-compose.yml down -v

psql-up:
	docker-compose -f docker-compose-psql.yml up --build -d

psql-down:
	docker-compose -f docker-compose-psql.yml down -v

prometheus-up:
	docker-compose -f docker-compose-prom.yml up --build -d

prometheus-down:
	docker-compose -f docker-compose-prom.yml down -v

minikube-deploy-up:
	minikube start
	minikube addons enable ingress

	kubectl apply -f cluster-deploy/deployment.yml
	kubectl apply -f cluster-deploy/node-port.yml
	kubectl apply -f cluster-deploy/ingress.yml

minikube-deploy-down:
	minikube delete