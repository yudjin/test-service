package adder

type numbers interface {
	int | float64 | string
}

type Adder[T numbers] struct {
	count int
}

func NewAdder[T numbers]() *Adder[T] {
	return &Adder[T]{count: 0}
}

func (s *Adder[T]) Sum(a, b T) T {
	s.count++
	return a + b
}

func (s *Adder[T]) GetSumRequestCount() int {
	return s.count
}

//
//
//
//
//
//

func Sum[T numbers](a,b T) T {
	return a + b
}
