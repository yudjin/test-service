package config

type Config struct {
	Port                    int     `yaml:"port"`
	GRPCPort                int     `yaml:"grpc_port"`
	DevURL                  string  `yaml:"dev_url"`
	DB                      string  `yaml:"db"`
	Version                 string  `yaml:"version"`
	Metrics                 Metrics `yaml:"metrics"`
	WorkersCount            int     `yaml:"workers_count"`
	TrackerDownloadInterval uint64  `yaml:"tracker_download_interval"`
}

// Prometheus metrics
type Metrics struct {
	SumIntMetricName           string `yaml:"sum_int_metric_name"`
	SumIntMetricDescription    string `yaml:"sum_int_metric_description"`
	SumFloatMetricName         string `yaml:"sum_float_metric_name"`
	SumFloatMetricDescription  string `yaml:"sum_float_metric_description"`
	SumStringMetricName        string `yaml:"sum_string_metric_name"`
	SumStringMetricDescription string `yaml:"sum_string_metric_description"`
}
