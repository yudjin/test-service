package workerpool

import (
	"bitbucket.org/yudjin/test-service/internal/downloader"
	"context"
	"github.com/sirupsen/logrus"
	"sync"
)

type worker struct {
	id   int
	jobs chan downloader.Job
}

func NewWorker(id int, jobs chan downloader.Job) *worker {
	return &worker{
		id:   id,
		jobs: jobs,
	}
}

func (w *worker) Start(wg *sync.WaitGroup, ctx context.Context) {
	wg.Add(1)

	go func() {
		logrus.Infof("worker %d start", w.id)
		defer wg.Done()

		select {
		case <-ctx.Done():
			logrus.Infof("worker %d stopped", w.id)
		case job := <-w.jobs:
			logrus.Infof("worker %d process job: %s", w.id, job.ID)
			if err := job.Func(job.Url, job.ID); err != nil {
				logrus.Errorf("err: %s", err)
			}
		}
	}()
}
