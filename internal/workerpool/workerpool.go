package workerpool

import (
	"bitbucket.org/yudjin/test-service/internal/downloader"
	"context"
	"sync"
)

type WorkerPool struct {
	pool int
	jobs chan downloader.Job
	wg   sync.WaitGroup

	cancelFunc context.CancelFunc
}

func NewWorkerPool(pool int) *WorkerPool {
	return &WorkerPool{
		jobs: make(chan downloader.Job),
		wg:   sync.WaitGroup{},
		pool: pool,
	}
}

func (p *WorkerPool) Init() {
	ctx, cnFunc := context.WithCancel(context.Background())
	p.cancelFunc = cnFunc

	for i := 1; i <= p.pool; i++ {
		NewWorker(i, p.jobs).Start(&p.wg, ctx)
	}
}

func (p *WorkerPool) AddJob(v downloader.Job) {
	p.jobs <- v
}

func (p *WorkerPool) Shutdown() {
	p.cancelFunc()

	p.wg.Wait()
}
