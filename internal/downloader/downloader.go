package downloader

import (
	"encoding/base64"
	"io"
	"net/http"
)

const (
	StatusReadyToDownload    = "ready to download"
	StatusDownloadInProgress = "in progress"
	StatusDownloadComplete   = "complete"
)

type WorkerPool interface {
	AddJob(v Job)
}

type Storage interface {
	GetReadyToDownloadList() ([]Download, error)
	AddDownload(urls []string) ([]Download, error)
	GetStatusByID(id string) (string, error)
	GetDownloadByID(id string) (Download, error)
	SetPayloadByID(id string, payload string) error
}

type Downloader struct {
	wp      WorkerPool
	storage Storage
	client  http.Client
}

type Download struct {
	ID      string `json:"id"`
	Status  string `json:"status"`
	Url     string `json:"url"`
	Payload string `json:"payload"`
}

type Job struct {
	ID   string
	Url  string
	Func func(url string, id string) error
}

func NewDownloader(wp WorkerPool, storage Storage) *Downloader {
	return &Downloader{wp: wp, storage: storage, client: http.Client{}}
}

func (d *Downloader) Download() error {
	list, err := d.storage.GetReadyToDownloadList()
	if err != nil {
		return err
	}

	for _, download := range list {
		d.wp.AddJob(Job{
			ID:   download.ID,
			Url:  download.Url,
			Func: d.downloadUrl,
		})
	}
	return nil
}

func (d *Downloader) AddDownload(urls []string) ([]Download, error) {
	return d.storage.AddDownload(urls)
}

func (d *Downloader) GetDownloadStatus(id string) (string, error) {
	return d.storage.GetStatusByID(id)
}

func (d *Downloader) GetDownload(id string) (Download, error) {
	return d.storage.GetDownloadByID(id)
}

func (d *Downloader) downloadUrl(url string, id string) error {
	resp, err := d.client.Get(url)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	data, err := io.ReadAll(resp.Body)
	if err != nil {
		return err
	}
	return d.storage.SetPayloadByID(id, base64.StdEncoding.EncodeToString(data))
}
