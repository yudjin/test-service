package db

import (
	"bitbucket.org/yudjin/test-service/internal/downloader"
	"database/sql"
	"github.com/huandu/go-sqlbuilder"
	"github.com/jmoiron/sqlx"
	uuid "github.com/satori/go.uuid"
)

type Download struct {
	ID      string         `db:"id"`
	Status  string         `db:"status"`
	Url     string         `db:"url"`
	Payload sql.NullString `db:"payload"`
}

func (s *Storage) GetReadyToDownloadList() ([]downloader.Download, error) {
	downloads := make([]Download, 0)
	err := sqlx.Select(s.ext, &downloads, `WITH tasks AS (SELECT id FROM downloads WHERE status = $1 limit 10 FOR UPDATE SKIP LOCKED)
    	UPDATE downloads SET status = $2 FROM tasks WHERE downloads.id = tasks.id RETURNING *`, downloader.StatusReadyToDownload,
		downloader.StatusDownloadInProgress)

	out := make([]downloader.Download, 0, len(downloads))
	for _, download := range downloads {
		out = append(out, downloader.Download{
			ID:      download.ID,
			Status:  download.Status,
			Url:     download.Url,
			Payload: download.Payload.String,
		})
	}
	return out, err
}

func (s *Storage) AddDownload(urls []string) ([]downloader.Download, error) {
	downloads := make([]Download, 0)
	ib := sqlbuilder.NewInsertBuilder()
	ib.InsertInto("downloads")
	ib.SetFlavor(sqlbuilder.PostgreSQL)
	ib.Cols("id", "status", "url")

	for _, url := range urls {
		ib.Values(uuid.NewV4().String(), downloader.StatusReadyToDownload, url)
	}
	insertQuery, insertArgs := ib.Build()
	insertQuery = insertQuery + "RETURNING *"
	err := sqlx.Select(s.ext, &downloads, insertQuery, insertArgs...)

	out := make([]downloader.Download, 0, len(downloads))
	for _, download := range downloads {
		out = append(out, downloader.Download{
			ID:      download.ID,
			Status:  download.Status,
			Url:     download.Url,
			Payload: download.Payload.String,
		})
	}
	return out, err
}

func (s *Storage) SetPayloadByID(id string, payload string) error {
	_, err := s.ext.Exec(`UPDATE downloads SET (status, payload) = ($1, $2) WHERE id = $3`, downloader.StatusDownloadComplete, payload, id)
	return err
}

func (s *Storage) GetStatusByID(id string) (string, error) {
	var status string
	err := sqlx.Get(s.ext, &status, `SELECT status FROM downloads WHERE id = $1`, id)
	return status, err
}

func (s *Storage) GetDownloadByID(id string) (downloader.Download, error) {
	var download Download
	err := sqlx.Get(s.ext, &download, `SELECT * FROM downloads WHERE id = $1`, id)

	return downloader.Download{
		ID:      download.ID,
		Status:  download.Status,
		Url:     download.Url,
		Payload: download.Payload.String,
	}, err
}
