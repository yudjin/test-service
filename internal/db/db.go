package db

import "github.com/jmoiron/sqlx"

type Storage struct {
	ext sqlx.Ext
}

func NewStorage(ext sqlx.Ext) *Storage {
	return &Storage{ext: ext}
}
