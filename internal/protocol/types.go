package protocol

type SumOfNumbersRequest struct {
	FirstNumber  any `json:"first_number"`
	SecondNumber any `json:"second_number"`
}

type SumOfNumbersResponse struct {
	Sum any `json:"sum"`
}
