package protocol

// swagger:parameters SumOfNumbersRequest
type SumOfNumbersRequestWrapper struct {
	//in:body
	Body SumOfNumbersRequest
}

// Ok response
// swagger:response SumOfNumbersResponse
type SumOfNumbersResponseWrapper struct {
	//in:body
	Body SumOfNumbersResponse
}

// Failure response
// swagger:response swaggerErr
type swaggerErr struct {
	// in:body
	Body struct {
		Meta Meta `json:"meta"`
	}
}
