package protocol

import (
	"encoding/json"
	"net/http"
)

type Meta struct {
	Code  int    `json:"code"`
	Error string `json:"error_message,omitempty"`
}

type Response struct {
	Meta Meta        `json:"meta"`
	Data interface{} `json:"data,omitempty"`
}

func WriteError(w http.ResponseWriter, code int, err string) {
	w.Header().Add("Content-Type", "application/json")

	w.WriteHeader(code)
	bytes, _ := json.Marshal(Response{
		Meta: Meta{
			Code:  code,
			Error: err,
		},
	})
	w.Write(bytes)
}

func WriteOk(w http.ResponseWriter, data interface{}) {
	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	bytes, _ := json.Marshal(Response{
		Meta: Meta{
			Code: http.StatusOK,
		},
		Data: data,
	})
	w.Write(bytes)
}
