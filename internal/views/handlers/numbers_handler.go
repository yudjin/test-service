package handlers

import (
	"bitbucket.org/yudjin/test-service/internal/adder"
	"bitbucket.org/yudjin/test-service/internal/metrics"
	"bitbucket.org/yudjin/test-service/internal/protocol"
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"gopkg.in/validator.v2"
	"io/ioutil"
	"net/http"
	"os"
	"time"
)

type NumbersHandler struct {
	metricCollector metrics.MetricCollector
	intAdder        *adder.Adder[int]
	floatAdder      *adder.Adder[float64]
	stringAdder     *adder.Adder[string]
}

func NewNumbersHandler(metricCollector metrics.MetricCollector, intAdder *adder.Adder[int], floatAdder *adder.Adder[float64],
	stringAdder *adder.Adder[string]) *NumbersHandler {
	return &NumbersHandler{metricCollector: metricCollector, intAdder: intAdder, floatAdder: floatAdder, stringAdder: stringAdder}
}

func (h *NumbersHandler) SetupRoutes(r *mux.Router) {
	//r.Path("/sum").Methods(http.MethodPost).HandlerFunc(h.SumOfNumbers)
	r.Path("/sum/int").Methods(http.MethodPost).HandlerFunc(h.SumInt)
	r.Path("/sum/float").Methods(http.MethodPost).HandlerFunc(h.SumFloat)
	r.Path("/sum/string").Methods(http.MethodPost).HandlerFunc(h.SumString)
	//
	r.Path("/sum/int/count").Methods(http.MethodGet).HandlerFunc(h.SumIntCount)
	r.Path("/sum/float/count").Methods(http.MethodGet).HandlerFunc(h.SumFloatCount)
	r.Path("/sum/string/count").Methods(http.MethodGet).HandlerFunc(h.SumStringCount)
}

type SumIntRequest struct {
	First  int `json:"first" validate:"nonzero"`
	Second int `json:"second" validate:"nonzero"`
}

type SumFloatRequest struct {
	First  float64 `json:"first" validate:"nonzero"`
	Second float64 `json:"second" validate:"nonzero"`
}

type SumStringRequest struct {
	First  string `json:"first" validate:"nonzero"`
	Second string `json:"second" validate:"nonzero"`
}

func (h *NumbersHandler) SumInt(w http.ResponseWriter, r *http.Request) {
	start := time.Now()
	var req SumIntRequest

	if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
		protocol.WriteError(w, http.StatusBadRequest, "request decode error")
		duration := time.Since(start)
		h.metricCollector.WriteSumIntMetric(http.StatusBadRequest, duration)
		return
	}
	defer r.Body.Close()

	requestValidator := validator.NewValidator()
	if err := requestValidator.Validate(req); err != nil {
		protocol.WriteError(w, http.StatusBadRequest, "can`t validate request")
		duration := time.Since(start)
		h.metricCollector.WriteSumIntMetric(http.StatusBadRequest, duration)
		return
	}

	out := h.intAdder.Sum(req.First, req.Second)

	protocol.WriteOk(w, out)

	duration := time.Since(start)
	h.metricCollector.WriteSumIntMetric(http.StatusOK, duration)
}

func (h *NumbersHandler) SumFloat(w http.ResponseWriter, r *http.Request) {
	start := time.Now()
	var req SumFloatRequest

	if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
		duration := time.Since(start)
		h.metricCollector.WriteSumFloatMetric(http.StatusBadRequest, duration)
		protocol.WriteError(w, http.StatusBadRequest, "request decode error")
		return
	}
	defer r.Body.Close()

	requestValidator := validator.NewValidator()
	if err := requestValidator.Validate(req); err != nil {
		protocol.WriteError(w, http.StatusBadRequest, "can`t validate request")
		duration := time.Since(start)
		h.metricCollector.WriteSumFloatMetric(http.StatusBadRequest, duration)
		return
	}

	out := h.floatAdder.Sum(req.First, req.Second)

	protocol.WriteOk(w, out)

	duration := time.Since(start)
	h.metricCollector.WriteSumFloatMetric(http.StatusOK, duration)
}

func (h *NumbersHandler) SumString(w http.ResponseWriter, r *http.Request) {
	start := time.Now()
	var req SumStringRequest

	if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
		protocol.WriteError(w, http.StatusBadRequest, "request decode error")
		duration := time.Since(start)
		h.metricCollector.WriteSumStringMetric(http.StatusBadRequest, duration)
		return
	}
	defer r.Body.Close()

	requestValidator := validator.NewValidator()
	if err := requestValidator.Validate(req); err != nil {
		protocol.WriteError(w, http.StatusBadRequest, "can`t validate request")
		duration := time.Since(start)
		h.metricCollector.WriteSumStringMetric(http.StatusBadRequest, duration)
		return
	}

	out := h.stringAdder.Sum(req.First, req.Second)

	protocol.WriteOk(w, out)

	duration := time.Since(start)
	h.metricCollector.WriteSumStringMetric(http.StatusOK, duration)
}

func (h *NumbersHandler) SumIntCount(w http.ResponseWriter, _ *http.Request) {
	out := h.intAdder.GetSumRequestCount()
	protocol.WriteOk(w, out)
}

func (h *NumbersHandler) SumFloatCount(w http.ResponseWriter, _ *http.Request) {
	out := h.floatAdder.GetSumRequestCount()
	protocol.WriteOk(w, out)
}

func (h *NumbersHandler) SumStringCount(w http.ResponseWriter, _ *http.Request) {
	out := h.stringAdder.GetSumRequestCount()
	protocol.WriteOk(w, out)
}

// swagger:operation POST /api/sum SumOfNumbersRequest
// ---
// consumes:
// - application/json
// produces:
// - application/json
// responses:
//
//	"200":
//	  "$ref": "#/responses/SumOfNumbersResponse"
//	"400":
//	  "$ref": "#/responses/swaggerErr"
func (h *NumbersHandler) SumOfNumbers(w http.ResponseWriter, req *http.Request) {

	const MaxBodyBytes = int64(65536)
	req.Body = http.MaxBytesReader(w, req.Body, MaxBodyBytes)
	payload, err := ioutil.ReadAll(req.Body)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Error reading request body: %v\n", err)

		w.WriteHeader(http.StatusServiceUnavailable)
		return
	}

	var numbers protocol.SumOfNumbersRequest
	err = json.Unmarshal(payload, &numbers)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Error parsing JSON: %v\n", err)

		w.WriteHeader(http.StatusBadRequest)
		return
	}

	firstNumberInt, okParseFirstNumber := numbers.FirstNumber.(int)
	secondNumberInt, okParseSecondNumber := numbers.SecondNumber.(int)
	if okParseFirstNumber && okParseSecondNumber {
		protocol.WriteOk(w, protocol.SumOfNumbersResponse{
			Sum: adder.Sum(firstNumberInt, secondNumberInt),
		})
		return
	} else {
		firstNumberFloat, okParseFirstNumber := numbers.FirstNumber.(float64)
		secondNumberFloat, okParseSecondNumber := numbers.SecondNumber.(float64)
		if okParseFirstNumber && okParseSecondNumber {
			protocol.WriteOk(w, protocol.SumOfNumbersResponse{
				Sum: adder.Sum(firstNumberFloat, secondNumberFloat),
			})
			return
		}
		w.WriteHeader(http.StatusInternalServerError)
	}
}
