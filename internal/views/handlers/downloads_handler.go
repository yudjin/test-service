package handlers

import (
	"bitbucket.org/yudjin/test-service/internal/downloader"
	"bitbucket.org/yudjin/test-service/internal/protocol"
	"encoding/json"
	"github.com/gorilla/mux"
	"net/http"
)

type Downloader interface {
	AddDownload(urls []string) ([]downloader.Download, error)
	GetDownloadStatus(id string) (string, error)
	GetDownload(id string) (downloader.Download, error)
}

type DownloadsHandler struct {
	downloader Downloader
}

type addDownloadRequest struct {
	Urls []string `json:"urls"`
}

type statusDownloadResponse struct {
	Status string `json:"status"`
}

func NewDownloadsHandler(downloader Downloader) *DownloadsHandler {
	return &DownloadsHandler{downloader: downloader}
}

func (h *DownloadsHandler) SetupRoutes(router *mux.Router) {
	router.Path("/downloads").Methods(http.MethodPost).HandlerFunc(h.AddDownload)
	router.Path("/downloads/{id}").Methods(http.MethodGet).HandlerFunc(h.GetDownload)
	router.Path("/downloads/{id}/status").Methods(http.MethodGet).HandlerFunc(h.Status)
}

func (h *DownloadsHandler) AddDownload(w http.ResponseWriter, r *http.Request) {
	var req addDownloadRequest

	if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
		protocol.WriteError(w, http.StatusBadRequest, "request decode error")
		return
	}
	defer r.Body.Close()

	out, err := h.downloader.AddDownload(req.Urls)
	if err != nil {
		protocol.WriteError(w, http.StatusInternalServerError, err.Error())
		return
	}

	protocol.WriteOk(w, out)
}

func (h *DownloadsHandler) GetDownload(w http.ResponseWriter, r *http.Request) {
	id, ok := mux.Vars(r)["id"]
	if !ok {
		protocol.WriteError(w, http.StatusBadRequest, "missing id")
		return
	}

	out, err := h.downloader.GetDownload(id)
	if err != nil {
		protocol.WriteError(w, http.StatusInternalServerError, err.Error())
		return
	}

	protocol.WriteOk(w, out)
}

func (h *DownloadsHandler) Status(w http.ResponseWriter, r *http.Request) {
	id, ok := mux.Vars(r)["id"]
	if !ok {
		protocol.WriteError(w, http.StatusBadRequest, "missing id")
		return
	}

	status, err := h.downloader.GetDownloadStatus(id)
	if err != nil {
		protocol.WriteError(w, http.StatusInternalServerError, err.Error())
		return
	}

	protocol.WriteOk(w, statusDownloadResponse{Status: status})
}
