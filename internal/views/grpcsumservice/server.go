package grpcsumservice

import (
	"context"
)

type Server struct {
}

func NewServer() *Server {
	return &Server{}
}

func (s *Server) SumOfTwoNumbers(_ context.Context, request *SumNumbersRequest) (*SumNumbersResponse, error) {
	return &SumNumbersResponse{SumNumbers: request.FirstNumber + request.SecondNumber}, nil
}
