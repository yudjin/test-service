package metrics

import (
	"fmt"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"net/http"
	"strconv"
	"time"
)

const prometheusResponseCodeLabel = "Code"

type MetricCollector interface {
	WriteSumIntMetric(responseCode int, time time.Duration)
	WriteSumFloatMetric(responseCode int, time time.Duration)
	WriteSumStringMetric(responseCode int, time time.Duration)
	GetMetricsHandler() http.Handler
}

type prometheusMetricCollector struct {
	sumIntMetric    *prometheus.HistogramVec
	sumFloatMetric  *prometheus.HistogramVec
	sumStringMetric *prometheus.HistogramVec
}

func NewPrometheusMetricCollector(sumIntMetricName, sumIntMetricDescription, sumFloatMetricName, sumFloatMetricDescription,
	sumStringMetricName, sumStringMetricDescription string) (*prometheusMetricCollector, error) {
	sumIntMetric := prometheus.NewHistogramVec(prometheus.HistogramOpts{
		Name: sumIntMetricName,
		Help: sumIntMetricDescription,
	}, []string{prometheusResponseCodeLabel})

	sumFloatMetric := prometheus.NewHistogramVec(prometheus.HistogramOpts{
		Name: sumFloatMetricName,
		Help: sumFloatMetricDescription,
	}, []string{prometheusResponseCodeLabel})

	sumStringMetric := prometheus.NewHistogramVec(prometheus.HistogramOpts{
		Name: sumStringMetricName,
		Help: sumStringMetricDescription,
	}, []string{prometheusResponseCodeLabel})

	if err := prometheus.Register(sumIntMetric); err != nil {
		return nil, fmt.Errorf("register metric %s", err)
	}
	if err := prometheus.Register(sumFloatMetric); err != nil {
		return nil, fmt.Errorf("register metric %s", err)
	}
	if err := prometheus.Register(sumStringMetric); err != nil {
		return nil, fmt.Errorf("register metric %s", err)
	}

	return &prometheusMetricCollector{sumIntMetric: sumIntMetric, sumFloatMetric: sumFloatMetric, sumStringMetric: sumStringMetric}, nil
}

func (c *prometheusMetricCollector) WriteSumIntMetric(responseCode int, time time.Duration) {
	c.sumIntMetric.WithLabelValues(strconv.Itoa(responseCode)).Observe(time.Seconds())
}

func (c *prometheusMetricCollector) WriteSumFloatMetric(responseCode int, time time.Duration) {
	c.sumFloatMetric.WithLabelValues(strconv.Itoa(responseCode)).Observe(time.Seconds())
}

func (c *prometheusMetricCollector) WriteSumStringMetric(responseCode int, time time.Duration) {
	c.sumStringMetric.WithLabelValues(strconv.Itoa(responseCode)).Observe(time.Seconds())
}

func (c *prometheusMetricCollector) GetMetricsHandler() http.Handler {
	return promhttp.Handler()
}
