// Package main API endpoints for test-service
//
// ExampleProject API
//
//	Schemes: https, http
//	BasePath: /
//	Version: 0.1

package main

import (
	"bitbucket.org/yudjin/test-service/internal/adder"
	"bitbucket.org/yudjin/test-service/internal/config"
	"bitbucket.org/yudjin/test-service/internal/db"
	"bitbucket.org/yudjin/test-service/internal/downloader"
	"bitbucket.org/yudjin/test-service/internal/metrics"
	"bitbucket.org/yudjin/test-service/internal/protocol"
	grpcsum "bitbucket.org/yudjin/test-service/internal/views/grpcsumservice"
	"bitbucket.org/yudjin/test-service/internal/views/handlers"
	"bitbucket.org/yudjin/test-service/internal/workerpool"
	"errors"
	"github.com/golang-migrate/migrate/v4"
	"github.com/golang-migrate/migrate/v4/database/postgres"
	_ "github.com/golang-migrate/migrate/v4/source/file"
	"github.com/jasonlvhit/gocron"
	"github.com/jmoiron/sqlx"
	"time"

	"fmt"
	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
	"google.golang.org/grpc"
	"google.golang.org/grpc/grpclog"
	"net"
	"net/http"
	"os"
	"os/signal"
	"syscall"

	"github.com/jinzhu/configor"
)

var (
	cfg config.Config

	workerPool *workerpool.WorkerPool
	conn       *sqlx.DB
)

func main() {
	if err := configor.Load(&cfg, "etc/config.yml"); err != nil {
		logrus.Fatal(err)
	}

	//worker pool
	workerPool = workerpool.NewWorkerPool(cfg.WorkersCount)
	workerPool.Init()
	defer workerPool.Shutdown()

	//postgres
	var err error
	conn, err = sqlx.Open("postgres", cfg.DB)
	if err != nil {
		logrus.Fatalf("connect db %s", err.Error())
	}

	conn.SetMaxIdleConns(10)
	conn.SetMaxOpenConns(10)

	driver, err := postgres.WithInstance(conn.DB, &postgres.Config{})
	if err != nil {
		logrus.Fatalf("migration driver %s", err.Error())
	}

	m, err := migrate.NewWithDatabaseInstance(
		"file://migrations",
		"postgres", driver)
	if err != nil {
		logrus.Fatalf("init migrations %s", err.Error())
	}

	err = m.Up()
	if err != nil && !errors.Is(err, migrate.ErrNoChange) {
		logrus.Fatalf("migrations %s", err.Error())
	}

	runAsAPI()
	runAsGRPC()
	runAsTracker()

	terminate := make(chan os.Signal)
	signal.Notify(terminate, syscall.SIGINT, syscall.SIGTERM) //nolint
	s := <-terminate
	logrus.Infof("service closed with signal: %s", s)
}

func runAsGRPC() {
	listener, err := net.Listen("tcp", fmt.Sprintf("0.0.0.0:%d", cfg.GRPCPort))

	if err != nil {
		grpclog.Fatalf("failed to listen: %v", err)
	}

	opts := []grpc.ServerOption{}
	grpcServer := grpc.NewServer(opts...)

	go func() {
		grpcsum.RegisterSumServiceServer(grpcServer, grpcsum.NewServer())
		grpcServer.Serve(listener)
	}()

}

func runAsAPI() {
	router := mux.NewRouter().StrictSlash(false)

	//init prometheus
	metricCollector, err := metrics.NewPrometheusMetricCollector(cfg.Metrics.SumIntMetricName, cfg.Metrics.SumIntMetricDescription,
		cfg.Metrics.SumFloatMetricName, cfg.Metrics.SumFloatMetricDescription, cfg.Metrics.SumStringMetricName, cfg.Metrics.SumStringMetricDescription)
	if err != nil {
		logrus.Fatalf("prometheus %s", err)
	}

	routeAPI(router, metricCollector)
	routeBasic(router, metricCollector)
	http.Handle("/", router)
	go func() {
		logrus.Infof("start listening at %d", cfg.Port)
		logrus.Fatal(http.ListenAndServe(fmt.Sprintf(":%d", cfg.Port), router))
	}()
}

func runAsTracker() {
	logrus.Infof("tracker start job")

	storage := db.NewStorage(conn)

	gocron.ChangeLoc(time.UTC)
	gocron.Start()

	d := downloader.NewDownloader(workerPool, storage)

	gocron.Every(cfg.TrackerDownloadInterval).Second().Do(func() {
		err := d.Download()
		if err != nil {
			logrus.Errorf("download error: %s", err)
		}
	})
}

func routeAPI(router *mux.Router, metricCollector metrics.MetricCollector) {
	apiRouter := router.PathPrefix("/api").Subrouter()

	intAdder := adder.NewAdder[int]()
	floatAdder := adder.NewAdder[float64]()
	stringAdder := adder.NewAdder[string]()

	numbersHandler := handlers.NewNumbersHandler(metricCollector, intAdder, floatAdder, stringAdder)
	numbersHandler.SetupRoutes(apiRouter)

	handlers.NewDownloadsHandler(downloader.NewDownloader(nil, db.NewStorage(conn))).SetupRoutes(apiRouter)
	apiRouter.StrictSlash(false)
}

func routeBasic(router *mux.Router, metricCollector metrics.MetricCollector) {
	//metrics client handler
	router.Handle("/metrics", metricCollector.GetMetricsHandler())

	// swagger:operation GET /version getVersion
	// summary: get app version: in form of branch-revision
	// ---
	router.HandleFunc("/version", func(w http.ResponseWriter, _ *http.Request) {
		protocol.WriteOk(w, cfg.Version)
	}).Methods("GET")

	// serve swagger
	router.HandleFunc("/swagger.json", func(w http.ResponseWriter, r *http.Request) {
		http.ServeFile(w, r, "./swagger.json")
	})

	router.PathPrefix("/swagger").Handler(
		http.StripPrefix("/swagger/", http.FileServer(http.Dir("./static/swagger"))))
}
