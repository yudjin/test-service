CREATE TYPE download_status AS ENUM ('ready to download', 'in progress', 'complete');

CREATE TABLE downloads (
    id      UUID UNIQUE NOT NULL,
    url     TEXT        NOT NULL,
    status  download_status NOT NULL,
    payload bytea
);

CREATE INDEX downloads_status_idx on downloads (status);


