FROM alpine:3.8
MAINTAINER Moroz Evgeny <yauheni.maroz@skyrocket.by>

RUN apk update && apk add ca-certificates
RUN apk add bash
COPY api /app/
COPY ./etc /app/etc/
COPY swagger.json /app/
COPY ./static /app/static/
WORKDIR /app
EXPOSE 8080
ENTRYPOINT ["/app/api"]